---
title: "Association Lucioles de Figeac"
order: 1
in_menu: true
---
![Sur un arrière plan blanc, le texte LUCIOLES en blanc avec une légère aura grise, affiché devant un nuage de globules verts plus ou moins transparents. Serait-ce un nuage de lucioles ?](/images/logo.png)

# Association Lucioles de Figeac

**Lucioles** est une association loi de 1901, indépendante et a-partisane.

**Lucioles** a pour but d'éveiller les publics aux questions environnementales et aux problématiques posées par le dérèglement climatique, via la création d'échanges et la mise en place d'actions d'accompagnement à la transition.

- ateliers pédagogiques « Fresque du Climat », « Fresque de la mobilité », etc ;
- spectacles, conférences gesticulées ;
- organisation avec nos partenaires du festival de la transition heureuse ;
- mise en lien des initiatives locales en faveur de la transition écologique ;
- tous outils, supports événement pour sensibiliser le public et les décideurs. 