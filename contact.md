---
title: "Contact"
order: 3
in_menu: true
---
La collégiale **Lucioles** est joignable à l'adresse suivante : 📝 [luciolesfigeac@gmail.com](mailto:luciolesfigeac@gmail.com)

Pour recevoir la **newsletter *mensuelle*** : [adhérez !](https://www.helloasso.com/associations/lucioles-figeac/adhesions/adhesion-a-l-association-lucioles-a-figeac). Vous retrouverez ainsi les initiatives locales qui œuvrent sur les questions environnementales, avec notamment un agenda des évènements Lucioles et de nos partenaires.

L'adhésion peut-être établie sur le site helloasso : [Adhésion à l'association Lucioles à Figeac](https://www.helloasso.com/associations/lucioles-figeac/adhesions/adhesion-a-l-association-lucioles-a-figeac)

![Sur un arrière plan blanc, le texte LUCIOLES en blanc avec une légère aura grise, affiché devant un nuage de globules verts plus ou moins transparents. Serait-ce un nuage de lucioles ?](/images/logo.png) 