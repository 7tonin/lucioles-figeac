---
title: "Activités"
order: 2
in_menu: true
---
# 📅 Prochainement #

## 2024 ##

### Mai ###

Festival de la transition heureuse sur le thème du *textile*
![L'affiche et le dépliant](https://www.lucioles-figeac.org/fth2024/infographies_transition-heureuse2024_web.jpg)

[Télécharger l'affiche (744 Kio)](https://www.lucioles-figeac.org/fth2024/affiche_transition_heureuse2024_A4.pdf)

[Télécharger le dépliant du festival de la transition heureuse sur le thème du textile (2 Mio)](https://www.lucioles-figeac.org/festival-transition-heureuse-2024-textile-programme.pdf)

# 🎯 Ces derniers mois  #

## 2024 ##

### Mars ###

- La rencontre Transition du 2 mars à l'Arrosoir
- Une nouvelle réunion d'organisation du festival sur le Textile
- Infos compostage au Compost Besombes : 
> le vendredi 8 mars, nous avons accueilli les 19 élèves d'une classe de CM1 de l'Ecole Chapou, qui vient composter ses biodéchets de goûter. L'objectif était de monter ensemble le 2ème kit de compostage, de leur expliquer le fonctionnement du compost et de leur partager la richesse et tous les avantages du compostage.
> 
> Nous avions donc préparé 3 ateliers auxquels tous les élèves ont participé :
> - Jeu du "Petit composteur"
> - Découverte de la faune du compost : repérage et identification des décomposeurs
> - Badigeon des planches avant montage
> 
> Tous les élèves se sont impliqués avec un bel enthousiasme et dans la bonne humeur. C'était un moment très plaisant à partager avec eux, les accompagnateurs, leur institutrice et quelques passants venus nous poser des questions et nous encourager.
> 
> Ils ont ensuite rejoint leur école et nous avons finalisé le montage des bacs à 3, entre les gouttes de pluie.
> 
> Un très grand merci à tous les participants de cette matinée riche !

### Février ###

- La rencontre Transition du 3 février à l'Arrosoir
- Une nouvelle réunion d'organisation du festival sur le Textile, qui commence à prendre forme 😍
- Une nouvelle fresque du climat par Astrid pour les jeunes de l'école ETRE
- Infos compostage :
> Compost place Besombes : après un premier transfert fin janvier (du bac de dépôt au bac de maturation), le bac s’est à nouveau rempli à grande vitesse ! La décision a été prise de mettre en place un nouveau kit à côté du premier. Le 8 mars prochain au matin, une classe de CM1 d’une école voisine viendra en renfort pour nous aider au montage de ce nouveau bac et découvrir les enjeux d’un compost partagé. En tous cas, c’est un succès pour ce nouveau compost, qui fonctionne très bien et qui compte très peu d’intrus.
> Tout l’enjeu est maintenant de poursuivre la mobilisation afin que les personnes qui viennent déposer puissent contribuer à l’animation de ce lieu partagé 😊.

### Janvier ###

- La rencontre Transition du 13 janvier à l'Arrosoir, associée à l'animation de la fresque de la biodiversité et d'un partage de galette 😋
- Nouvelles fresques du climat junior en écoles, cette fois-ci au collège Saint-Louis à Capdenac-Gare, bravo à Astrid, Mathilde, Boris et Cédric pour l'animation ! 
Et pour l'école ÊTRE lundi 29 janvier par Astrid !
- Une nouvelle réunion d'organisation du festival sur le Textile, festival qui aura lieu à la Pentecôte 😍
- La fresque du climat géante le dimanche 28 janvier après-midi à Cuzac : 25 personnes adultes et enfants (soit 10% de la population du village ! ) qui ont réalisé la fresque en même temps dans un esprit convivial ✨ Merci aux animateurs Astrid, Mathilde et Cédric ! <br>![Les participants à la fresque du climat cuzacoise](https://www.lucioles-figeac.org/images/IMG_20240128_194642_r.jpg)[La Dépêche du Midi > Accueil > France - Monde > Environnement > Cuzac. Un atelier collectif pour explorer les enjeux climatiques](https://www.ladepeche.fr/2024/02/01/un-atelier-collectif-pour-explorer-les-enjeux-climatiques-11735948.php)

## 2023 ##

### Décembre ###

- La rencontre Transition du 2 décembre à l'Arrosoir
- Nouvelle fresque du climat le 15 décembre par Astrid !
- La refonte du site web par Antonin et Léocadie 🤗 lucioles-figeac.org
- L'atelier de cuisine vegan par Ursula et Yves, bravo ♥️🍎
- Coté compost partagés en décembre nous avons fait les deuxièmes retournements pour les compost de la rue d’Aujou et du jardin Sully, et ajouté un deuxième kit rue du Claux et du côté de la place Besombes, un premier apéro partagé a rassemblé une dizaine de personnes et nous allons réaliser le 1er transfert début janvier 😊.
Janvier c’est le moment des bonnes résolutions ! Alors c’est décidé : si j’utilise un composteur partagé je participe à mon échelle ! Un coup de fourche pour mélanger mes apports, j’ajoute de la matière brune à chaque fois et je donne un coup de main pour les transferts pour que ce soit plus sympa et que les référents ne se sentent pas trop seul ! 🥰

### Novembre ###

- La rencontre Transition du 4 novembre à l'Arrosoir
- Mathilde, Astrid et Cédric qui sont allés sensibiliser des collégiens à Decazeville, fresque du climat Enfants avec un lien Lucioles-Derrière le Hublot. Bravo ! 🤗
- Astrid qui a aussi fait une nouvelle fresque du climat Enfants à l'école Etre Figeacteurs. Bravo bis !
- La soirée ciné-débat sur l'agriculture qui a fait salle comble à l'Arrosoir !
- Une nouvelle réunion d'organisation du 2ème festival de la Transition Heureuse sur le Textile 🧥
- La co-organisation d'une 2ème édition de la Fête de l'Arbre à Corn qui a su rassembler un public nombreux 😍

### Octobre ###

- La rencontre Transition du 7 octobre à l'Arrosoir
- Jean-Luc et Astrid qui sont allés sensibiliser des CE2-CM2 à Cajarc dans le cadre du festival Célotomne à Cajarc. Bravo ! 🤗
- Astrid qui a aussi fait une fresque du climat Enfants à l'école Etre Figeacteurs. Bravo bis !
- La première réunion d'organisation du 2ème festival de la Transition Heureuse sur le Textile 🧥

### Septembre ###

- Le forum des associations début septembre qui nous a fait (re)connaître au grand public.
- Une Rencontre transition pour faire connaissance et lancer plein d'idées d'actions !
- Une sensibilisation Lucioles lors de la journée de rentrée des lycéens du lycée Champollion. Bravo à Marthe, Luc, Astrid et Antonin pour avoir semer de nouvelles graines dans les têtes des jeunes figeacois.e.s 😍
- Le spectacle de Cédric "Dis papa, est-ce que les calottes sont cuites?" qui a fait salle comble à Felzins 😎
- La première fresque du climat avec Astrid à l'animation !  Et celle de Mathilde pour l'école ETRE de Figeacteurs ! Bravo !
- Côté composteurs, ça avance également : jardin Sully, rue du Claux, Centre social, place Besombes, rue d'Aujou, ils se multiplient ! Un composteur devrait arriver à Gréalou également ce mois-ci pour la cantine et les habitants. Une bonne pratique à réaliser, indispensable avec l'obligation légale arrivant début 2024 👍
D'ailleurs le Grand Figeac et le Syded encouragent la pratique avec la distribution gratuite de kits via l'opération “Je me mets au compost !” : 9 dates sur l’ensemble du territoire. Inscrivez-vous sur www.grand-figeac.fr
- Et en prime un article pour annoncer notre rentrée : https://medialot.fr/grand-figeac-lassociation-lucioles-fait-sa-rentree/ 